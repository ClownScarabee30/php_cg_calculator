<?php

declare(strict_types=1);

//Récupération du form GET
$quantity = 0;
$CG_val = 0;
$B_val = 0;
if ((isset($_GET['quantity']) && ctype_digit($_GET['quantity'])) and
    (isset($_GET['CG_val']) && ctype_digit($_GET['CG_val'])) and
    (isset($_GET['B_val']) && ctype_digit($_GET['B_val']))and
    (isset($_GET['F_val']) && ctype_digit($_GET['F_val']))and
    isset($_GET['typeFioul']) && ctype_digit($_GET['typeFioul'])){
    $quantity = intval($_GET['quantity']);
    $CG_val = intval($_GET['CG_val']);
    $B_val = intval($_GET['B_val']);
    $F_val = intval($_GET['F_val']);
    $typeFioul = intval($_GET['typeFioul']);
    if($CG_val < 1000){
        $CG_val = 1000;
    }
    $html = <<<HTML

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
             <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                         <meta http-equiv="X-UA-Compatible" content="ie=edge">
             <title>CG Calculator</title>
             <link href="style.css" rel="stylesheet">

</head>
<body>
    <h1>CG Calculator</h1>

<div class="formu">
<h3>Summary:</h3>
<li>
    <ul>Amount of Graphic Card : {$quantity}</ul>  
    <ul>Graphic Card value: {$CG_val} ₽</ul>
    <ul>BTC value : {$B_val} ₽</ul>
</li>

HTML;
    $J = 'd';
    $H = 'h';
    $fioul = $F_val/$typeFioul; //calcul du prix du  fioul / h

    $BperHour=1/(145000/(1+($quantity-1)*0.041225)/3600); // Taux de Bitcoin / heure avec la config actuelle
    $HourNeeded=145000/(1+($quantity-1)*0.041225)/3600; // Heure requise pour produire un Bitcoin avec la config actuelle
    $RperHour= $B_val/$HourNeeded; //Taux de Rouble Par Heure
    $CG_Total = $quantity*$CG_val; //Cout total des Carte Graphique en Rouble
    $RperHourNet = $RperHour - $fioul;

    //Identification de la bitcoinFarm Requise
    if ($quantity < 11){
        $level = 'Level 1 : 500k';
        $price = 500000;
    }else{
        if ($quantity <26){
            $level = 'Level 2 : 1 000k';
            $price = 1000000;

        }else{
            $level = 'Level 3 : 2 000k + Solar System';
            $price = 2000000;

        }
    }

    //Calcul des seuils de Rentabilité
    //Temps de remboursement bitcoinFarm
    $Remb_CG= $CG_Total/$RperHourNet;
    $Remb_CG_D = 0;
    while ($Remb_CG > 24) {
        $Remb_CG = $Remb_CG - 24;
        $Remb_CG_D = $Remb_CG_D + 1;
    }

    //Temps de remboursement des CG
    $Remb_Farm= $price/$RperHourNet;
    $Remb_Farm_D = 0;
    while ($Remb_Farm > 23){
        $Remb_Farm = $Remb_Farm -24;
        $Remb_Farm_D = $Remb_Farm_D +1;
    }
    //Temps Total
    $Remb_Total_D = $Remb_CG_D + $Remb_Farm_D;
    $Remb_Total = $Remb_Farm + $Remb_CG;
    while ($Remb_Total > 23){
        $Remb_Total = $Remb_Total -24;
        $Remb_Total_D = $Remb_Total_D +1;
    }
    // Profit/h - Fioul

    // Arrondissement des Résultats
    $fioul = round($fioul,2);
    $HourNeeded = round($HourNeeded,2);
    $BperHour = round($BperHour,2);
    $RperHour = round($RperHour,2);
    $Remb_Farm = round($Remb_Farm,0);
    $Remb_CG = round($Remb_CG,0);
    $Remb_Total = round($Remb_Total,0);
    $RperHourNet = round($RperHourNet,2);
    $RperHourNetD = $RperHourNet*24;

    //correction
    if ($Remb_CG < 0 and $Remb_Farm <0){
        $Remb_CG = 'NO PROFITABLE ';
        $Remb_Farm='NO PROFITABLE ';
        $Remb_Total= 'NO PROFITABLE ';
        $Remb_Total_D = '';
        $Remb_Farm_D = '';
        $Remb_CG_D = '';

        $J = '';
        $H = '';
    }

    $html .= <<<HTML
    <h3>Bitcoin Farm needed : {$level}</h3>
    <li>
    <ul>Mining rate : {$BperHour}Btc/h</ul>
    <ul>Mining Time : {$HourNeeded}h/Btc</ul>   
    <ul>Ruble per Hour gross : {$RperHour}₽/h </ul>
</li>
<h3>External Cost</h3>
<li>
<ul>Fuel ({$F_val}₽/ {$typeFioul}h) :  {$fioul}₽/h</ul>
<ul>Bitcoin Farm : {$price}₽</ul>
<ul>Total GC value : {$CG_Total}₽</ul>
</li>
<h3>Rentability</h3>
<li>
    <ul>Required time to make the Bitcoin Farm profitable : {$Remb_Farm_D} {$J} {$Remb_Farm} {$H}</ul>
    <ul>Required time to make the GC profitable {$Remb_CG_D} {$J} {$Remb_CG} {$H}</ul>
    <ul>Required time to being fully profitable: {$Remb_Total_D} {$J} {$Remb_Total} {$H}</ul>
    <ul>Profit Per Hour: {$RperHourNet} ₽/h</ul>
    <ul>Profit Per Day: {$RperHourNetD} ₽/D</ul>
</li></div>
<footer>
    <li>
        <ul>
            Last Update : May 2022
        </ul>

        <ul>
           | Made By <a href="https://www.instagram.com/chinoah51/">Noah GALLOIS</a>
        </ul>
        <ul>
            | CopyLeft
        </ul>

        <ul>
            <a href="https://gitlab.com/ClownScarabee30/php_cg_calculator">| Link to the GitLab Project</a>
        </ul>
                    <ul> <a href="CG_Calculator/index.html">| Back to Home</a></ul>

    </li>
</footer>
HTML;


}else{
    header("location: /CG_Calculator/indexEN.html");
    exit;
}
echo $html;
